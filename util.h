//
// Created by Rudy on 08.01.2019.
//

#ifndef ZGADNIJ_UTIL_H
#define ZGADNIJ_UTIL_H
#include <limits>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <random>

template <class myType>
    myType generateQuest(){
    myType min = std::numeric_limits<myType>::lowest();
    myType max = std::numeric_limits<myType>::max();
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<> dis(min, max);

    return dis(gen);

}
template<typename fType>
fType generateQuestf() {
    std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<> pos_distrib(std::numeric_limits<fType>::min(), std::numeric_limits<fType>::max());
        std::uniform_real_distribution<> neg_distrib(std::numeric_limits<fType>::lowest(), 0);
        std::bernoulli_distribution sign_distrib{ 0.5 };
        return sign_distrib(gen) ? pos_distrib(gen) : neg_distrib(gen);
}

template<> inline float generateQuest<float>() {
    return generateQuestf<float>();
}

template<> inline double generateQuest<double>() {
    return generateQuestf<double>();
}

template<> inline long double generateQuest<long double>() {
    return generateQuestf<long double>();
}

#endif //ZGADNIJ_UTIL_H
