//
// Created by Rudy on 08.01.2019.
//

#ifndef ZGADNIJ_QUESTS_H
#define ZGADNIJ_QUESTS_H

#include <vector>
#include "util.h"

template <class myType>
class Quests{
public:
    typedef typename std::vector<myType>::iterator iterator;
    explicit Quests(int i){
        if(i<0) throw std::bad_alloc();
        this->questions.reserve(i);
        for (int k=0;k<i;++k)this->questions.push_back(generateQuest<myType>());
    }
    ~Quests()= default;
    myType operator[](int index){ return this->questions[index];}
    iterator begin(){ return this->questions.begin();}
    iterator end(){ return this->questions.end();}
    int size(){
        return this->questions.size();}
    void newQuestion(){
        this->questions.push_back (generateQuest<myType>());
    }


private:
    std::vector<myType> questions;
};



#endif //ZGADNIJ_QUESTS_H
