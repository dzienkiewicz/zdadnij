//
// Created by Rudy on 08.01.2019.
//

#ifndef ZGADNIJ_PLAYER_H
#define ZGADNIJ_PLAYER_H
#include <iostream>
#include <random>
#include <limits>
#include "util.h"
template <class myType>
myType generatevalue(myType min,myType max){
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<> dis(min, max);

    return dis(gen);

}
template<typename fType>
fType generatevaluef(fType min,fType max) {
    std::random_device rd;
    std::mt19937 gen(rd());

    std::uniform_real_distribution<> distrib(min, max);

    return static_cast<float>(distrib(gen));
}

template<> inline float generatevalue<float>(float min, float max) {
    return generatevaluef<float>(min,max);
}

template<> inline double generatevalue<double>(double min, double max) {
    return generatevaluef<double>(min, max);
}

template<> inline long double generatevalue<long double>(long double min, long double max) {
    return generatevaluef<long double> (min,max);}

static unsigned counter=0;
class Player {
public:
    Player();
    unsigned GetID(){ return this->id;}
    template<typename myType>
            void feedback(int i){
        feedbackvalue=i;
        if(i==2) start=true;
    }
    template<typename myType>
            myType guess(){
        static myType minguess = std::numeric_limits<myType>::lowest();
        static myType maxguess = std::numeric_limits<myType>::max();
        static myType lastAnwser;
        if(start){
            minguess = std::numeric_limits<myType>::lowest();
            maxguess = std::numeric_limits<myType>::max();
            start= false;
        }
        if(feedbackvalue==-1){ minguess=lastAnwser;}
        else if (feedbackvalue==1){maxguess=lastAnwser;}
        lastAnwser=generatevalue<myType>(minguess,maxguess);
        return lastAnwser;
            }


private:
    unsigned id;
    bool start=true;
    int feedbackvalue;
};


#endif //ZGADNIJ_PLAYER_H
